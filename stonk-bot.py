#!/usr/bin/python3.7
import logging
import json
import discord
from discord_webhook import DiscordWebhook, DiscordEmbed
import yfinance as yf
import os

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

formatter = logging.Formatter('%(asctime)s %(levelname)s %(name)s %(lineno)d %(message)s')

file_handler = logging.FileHandler('stonk_bot.log')
file_handler.setLevel(logging.ERROR)
file_handler.setFormatter(formatter)

stream_handler = logging.StreamHandler()
stream_handler.setFormatter(formatter)

logger.addHandler(file_handler)
logger.addHandler(stream_handler)

dirname = os.path.dirname(__file__)

config_file = os.path.join(dirname, "./src/stonk-bot.conf")
data_file = os.path.join(dirname, "./src/stonk-bot.json")


with open(config_file, 'r', encoding='utf8') as f:
    data = json.load(f)
    webhook_url = data['webhook']

with open(data_file, 'r', encoding='utf8') as f:
    data = json.load(f)

for stonk in data['stonks']:
    ticker = yf.Ticker(stonk).info
    try:
        diff = (ticker['ask'] - data['stonks'][stonk]['last'])/ticker['ask']
    except Exception as e:
        logger.exception(f'Problem with ticker {stonk}, {ticker}')
    else:
        logger.info(f"Ticker: {stonk}, Last: {data['stonks'][stonk]['last']}, Ask: {ticker['ask']}, Diff: {diff}")
        if abs(diff) > 0.01:
            webhook = DiscordWebhook(url=webhook_url)
            embed = DiscordEmbed(title=ticker['shortName'], description=f"{stonk} endring siste 15 min")
            embed.set_thumbnail(url=ticker['logo_url'])
            embed.add_embed_field(name="Endring", value=f"{round(100*diff,2)} %", inline=True)
            embed.add_embed_field(name="Fra", value=f"{ticker['currency']} {data['stonks'][stonk]['last']}", inline=True)
            embed.add_embed_field(name="Til", value=f"{ticker['currency']} {ticker['ask']}", inline=True)
            #content=f"{ticker['shortName']} har endra seg {100*diff}% siste 15 minuttene! Fra {ticker['currency']} {data['stonks'][stonk]['last']} til {ticker['currency']} {ticker['ask']}")
            webhook.add_embed(embed)
            response = webhook.execute()
        data['stonks'][stonk]['last'] = ticker['ask']

with open(data_file, 'w', encoding='utf8') as f:
    data = json.dump(data, f)
